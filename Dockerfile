# Main stage
FROM docker.io/aruw/mcb-2020-gitlab-ci:2025-01-19.1

# Install additional APT packages
RUN apt-get update -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get install -qq --no-install-recommends \
    bash-completion \
    build-essential \
    curl \
    doxygen \
    gdb-multiarch \
    libboost-all-dev \
    libncurses6 \
    # libstdc++-arm-none-eabi-newlib \
    nano \
    python-is-python3 \
    sudo \
    vim \
    && rm -rf /var/lib/apt/lists/*

# Add default non-root user to sudoers and switch
RUN echo ubuntu ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/ubuntu \
    && chmod 0440 /etc/sudoers.d/ubuntu
USER ubuntu

# Ensure that the non-root user owns .vscode-server dir. Otherwise, mounting the extensions
# volume will create the directory as root, causing vscode to fail when loading.
RUN mkdir -p /home/ubuntu/.vscode-server/extensions/