IMAGE_VERSION := "$(shell ./get-new-version.sh)"

build:
	time docker build . --tag aruw/mcb-dev

publish:
	docker tag aruw/mcb-dev aruw/mcb-dev:$(IMAGE_VERSION)
	docker push aruw/mcb-dev:$(IMAGE_VERSION)
