# aruw-mcb-dev-container

A Docker container which contains all the tools needed for working with the ARUW MCB codebase. Recommended for use with vscode's _Remote - Containers_ extension.

TODOs and known limitations:
- Customizations such as prompts and aliases aren't applied to the container by default, although you can re-apply them manually and they should "stick".

## Building and publishing the container image

_Make sure you have `jq` installed. On Ubuntu: `sudo apt install jq`._

Images are versioned by the date they were created, with an incrementing numeric suffix if there
are multiple builds on the same day. For example, `2020-08-25.3`.

First, make your Dockerfile changes and build the new image:
```bash
make build
```

The image is now locally tagged as `aruw/mcb-dev:latest`. If it works like you expect, publish the
new image:

```bash
make publish
```

`publish` will automatically pick up the current date from the system's clock and appropriate suffix
from Docker Hub.